package s;

public class MagicWord {

	private static final String[] WORDS = {"tester","player","gamestock","films"
			,"restricted","personaje","biblioteca"
			,"homenaje","computer","playstation",};
	private boolean[] guessed;
	private String hiddenWord;
	
	public MagicWord() {
		int numAlea = (int) (Math.random() * WORDS.length);
		hiddenWord = WORDS[numAlea];
		guessed = new boolean[hiddenWord.length()];
		for (int i = 0; i < guessed.length; i++) {
			guessed[i] = false;
			}
	}
	@Override
	public String toString() {
		String s = "";
		for (int i = 0; i < guessed.length; i++) {
				if (guessed[i] == false) {
					s = s+ "_";
				} else {
					s=s+ hiddenWord.toUpperCase().charAt(i);
				}
		}
		return (s);
	}
	
	public char checkLetter(char letter) {
		for (int i = 0; i < guessed.length; i++) {
			if (letter == hiddenWord.charAt(i)) {
				guessed[i]= true;
			}
		}
		return 0;
	}
	
	public boolean checkWin() {
		for (int i = 0; i < guessed.length; i++) {
			if (!guessed[i]) {
				return false;
			}
		}
		return true;
	}
}