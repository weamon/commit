package s;

import java.util.*;
public class WordGuess {
	
	public static final int MAX_SHOOTS = 10;

	public static void main(String[] args) {
		char letter;
		boolean gameOver = false;
		MagicWord magicWord = new MagicWord();
		int atemp = 0;
		
		Scanner scan = new Scanner(System.in);
		
		int cntr = 0;
		while (!gameOver)  {
			if (cntr==MAX_SHOOTS) {
				gameOver = true;
				System.out.println("You lose!!! ELM.exe");
			} else {
				System.out.println(magicWord);
				System.out.println("Enter a letter");
				System.out.println("Turn: " + atemp);
				letter = scan.next().charAt(0);
				magicWord.checkLetter(letter);
				atemp++;
				if (magicWord.checkWin()) {
					System.out.println("The word is: " + magicWord);
					System.out.println("You're a knegrejo");
					break;
				} else {
					cntr++;
				}
			}
		}
	}

}
